from rest_framework import serializers
from .models import OrganizationOrders001, Organization


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrganizationOrders001
        fields = ('id', 'mobile_number', 'organization', 'document_type', 'document_number', 'order_status')


    def create(self, validated_data):
        """
        Create and return a new `Order` instance, given the validated data.
        """
        return OrganizationOrders001.objects.create(**validated_data)


class OrderDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrganizationOrders001
        fields = ('id', 'order_status', 'order_udatetime')

    def update(self, instance, validated_data):
        """
        Update and return an existing `Order` instance, given the validated data.
        """
        '''instance.organization = validated_data.get('organization', instance.organization)
        instance.mobile_number = validated_data.get('mobile_number', instance.mobile_number)
        instance.document_type = validated_data.get('document_type', instance.document_type)
        instance.document_number =  validated_data.get('document_number', instance.document_number)'''
        instance.order_udatetime = validated_data.get('order_udatetime', instance.order_udatetime)
        instance.order_status = validated_data.get('order_status', instance.order_status)
        instance.save()
        return instance


class InformDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrganizationOrders001
        fields = ('id',
            'order_status',
            'order_udatetime',
            'probability_default',
            'upper_limit',
            'lower_limit',
            'subscriber_firstname',
            'subscriber_lastname',
            'scoring_data_id',
            'algorithm_id')

    def update(self, instance, validated_data):
        """
        Update and return an existing `Order` instance, given the validated data.
        """
        instance.probability_default = validated_data.get('probability_default', instance.probability_default)
        instance.upper_limit = validated_data.get('upper_limit', instance.upper_limit)
        instance.lower_limit = validated_data.get('lower_limit', instance.lower_limit)
        instance.subscriber_firstname =  validated_data.get('subscriber_firstname', instance.subscriber_firstname)
        instance.subscriber_lastname =  validated_data.get('subscriber_lastname', instance.subscriber_lastname)
        instance.scoring_data_id =  validated_data.get('scoring_data_id', instance.scoring_data_id)
        instance.algorithm_id =  validated_data.get('algorithm_id', instance.algorithm_id)
        instance.order_udatetime = validated_data.get('order_udatetime', instance.order_udatetime)
        instance.order_status = validated_data.get('order_status', instance.order_status)
        instance.save()
        return instance

