# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Organization, OrganizationUserProfile001, Tariff, OrganizationOrders001 #, OrganizationConsent001
# Register your models here.

class OrganizationAdmin(admin.ModelAdmin):
                list_display = ('organization_name', 'organization_type', 'organization_code', 'organization_krapin', 'tariff', 'organization_contactname', 'organization_contactnumber')

class OrganizationUserProfile001Admin(admin.ModelAdmin):
				list_display = ('user', 'organization', 'role')

class TariffAdmin(admin.ModelAdmin):
				list_display = ('tariff_name', 'unit_price', 'tax_rate')

#This will be deleted later---------------------------------------------------------------------------
class OrganizationOrders001Admin(admin.ModelAdmin):
				list_display = ('mobile_number', 'order_status', 'order_datetime', 'scoring_data_id', 'probability_default', 'lower_limit', 'upper_limit', 'score_datetime')



admin.site.register(Organization, OrganizationAdmin)
admin.site.register(OrganizationUserProfile001, OrganizationUserProfile001Admin)
admin.site.register(Tariff, TariffAdmin)

#This will be deleted later---------------------------------------------------------------------------
admin.site.register(OrganizationOrders001, OrganizationOrders001Admin)
