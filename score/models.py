# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User #added line to support User
from django.utils import timezone
from registration.signals import user_registered #added line to support User

from django.db.models.signals import post_save
from django.dispatch import receiver
from score.tasks import sendOrderTask
import uuid


# Create your models here.

class Tariff(models.Model):
	tariff_name=models.CharField(max_length=50)
	unit_price=models.IntegerField()
	tax_rate=models.DecimalField(decimal_places=2, max_digits=8)

	def __unicode__(self):
		return str(self.tariff_name)
	

class Organization(models.Model):
	organization_name=models.CharField(max_length=200, unique=True)
	organization_type=models.CharField(max_length=50)
	organization_code=models.CharField(max_length=6, unique=True)
	organization_krapin=models.CharField(max_length=11)
	organization_contactname=models.CharField(max_length=200)
	organization_contactnumber=models.IntegerField()
	tariff=models.ForeignKey("Tariff", on_delete=models.SET_NULL,
		blank=True,
		null=True,
		)

	def __unicode__(self):
		return str(self.organization_name)

class OrganizationUserProfile001(models.Model):
	user=models.OneToOneField(User, unique=True)
	organization=models.ForeignKey("Organization")
	role=models.CharField(max_length=50, blank=True, null=True)


	def __unicode__(self):
	    return str(self.user)	

class OrganizationOrders001(models.Model):
	REBATE_OPTIONS = (
		('Y', 'Yes'),
		('N', 'No'),
		)
	DOCUMENT_TYPE_OPTIONS = (
		('NATIONAL ID', 'national_id'),
		('PASSPORT', 'passport'),
		)
	STATUS_DESCRIPTION = (
		('INITIATED','order_initiated'),
		('ACCEPTED','order_accepted'),
		('SEEKING CONSENT','seek_consent'),
		('CONSENT DENIED','consent_denied'),
		('CONSENT TIMEOUT','consent_timeout'),
		('INSUFFICIENT BALANCE','insufficient_balance'),
		('CONSENTED','consent_given'),
		('COMPLETED','order_completed'),
		('CANCELLED','order_cancelled')
		)
	mobile_number=models.BigIntegerField()
	document_type=models.CharField(max_length=11, choices=DOCUMENT_TYPE_OPTIONS, default='national_id')
	document_number=models.CharField(max_length=8)
	order_status=models.CharField(max_length=20, choices=STATUS_DESCRIPTION, default='order_initiated')
	order_datetime=models.DateField(default=timezone.now)
	order_udatetime=models.DateField(null=True, blank=True)
	order_rebate_status=models.CharField(max_length=1, choices=REBATE_OPTIONS, default='N')
	scoring_data_id=models.CharField(max_length=200, default="Processing")
	probability_default=models.DecimalField(decimal_places=2, max_digits=5, default=0.00)
	upper_limit=models.IntegerField(default=0)
	lower_limit=models.IntegerField(default=0)
	score_datetime=models.DateField(null=True, blank=True)
	algorithm_id=models.CharField(max_length=200, default="Processing")
	subscriber_firstname = models.CharField(max_length=200, default="Processing")
	subscriber_lastname = models.CharField(max_length=200, default="Processing")
	organization=models.ForeignKey("Organization")
	
	class Meta:
		ordering = ['-id']


	def __unicode__(self):
		return str(self.mobile_number)

	

'''def user_registered_callback(sender, user, request, **kwargs):
    profile = OrganizationUserProfile001(user = user)
    profile.organization_name = str(request.POST["organization_name"])
    #profile.role = str(request.POST["role"])
    profile.save()
 
user_registered.connect(user_registered_callback)'''


'''@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
    	profile = OrganizationUserProfile001.objects.create(user=kwargs.get('instance'))
    	profile.organization = instance.organization_id
    	profile.save()'''
    	

@receiver(post_save, sender=OrganizationOrders001)
def getOrderData(sender, instance, created, **kwargs):
	if created:
		orderID = instance.id
		MSISDN = instance.mobile_number
		documentType = instance.document_type
		documentNumber = instance.document_number
		requestDate =  instance.order_datetime
		for requester in Organization.objects.filter(organization_name=instance.organization):
			organizationID = requester.organization_code
			organizationName =  requester.organization_name
		if documentType == 'NATIONAL ID':
			convertDocumentType = 'National Id Card'
		else:
			convertDocumentType = documentType

		return sendOrderTask.delay(orderID, organizationID, organizationName, convertDocumentType, documentNumber, MSISDN, requestDate)

