from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^orders/', views.orders, name='orders'),
    url(r'^purchase/', views.purchase, name='purchase'),
    url(r'^detail/(\d+)/', views.detail, name='detail'),
    url(r'^detail/report/(\d+)/', views.report, name='report'),
    url(r'^order/', views.Order.as_view(), name = 'order'),
    url(r'^orderdetail/(?P<pk>\d+)/$', views.OrderDetail.as_view(), name = 'orderdetail'),
    url(r'^inform/', views.inform, name='inform'),
    url(r'^informdetail/(?P<pk>\d+)/$', views.InformDetail.as_view(), name='informdetail'),
    url(r'^balance/', views.balance, name='balance'),
]
